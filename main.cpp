#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <cstring>
#include <vector>
#include <bits/stdc++.h>
#include <algorithm>
#include <array>

const int MAX = 26;


std::string stvoriUlaznePodatke() { 
  char abc[MAX] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                    'h', 'i', 'j', 'k', 'l', 'm', 'n',  
                    'o', 'p', 'q', 'r', 's', 't', 'u', 
                    'v', 'w', 'x', 'y', 'z' }; 
  
  std::string znak; 
  znak += abc[rand() % MAX];   
  return znak; 
} 

bool pronađiNiz(std::array<char,1000> struktura2, std::string str, int i, int j) {
  int n = str.length();

  char char_array[n+1];
  strcpy(char_array, str.c_str());
  for (int x = 0; x < sizeof(char_array); x++) { 
    char_array[x] = str[x]; 
  } 
  if(i < 1000) {
    if(struktura2[i] == char_array[j]) {
      j++;
      if(j == 3) {
        return true;
      } 
      else {
        return pronađiNiz(struktura2, str, i+1, j);
      }
    }
    else {
      return pronađiNiz(struktura2, str, i+1, 0);
    }
  }
  return false;
}

int main() {
  //#|#|#|#|#|#|#|#|#|# S E G M E N T 1 #|#|#|#|#|#|#|#|#|#
  srand(time(NULL)); 

  std::ofstream ofile("ulaz.csv");
  for (int counter = 0; counter < 10000; counter++) {
    if(counter == 0 || counter == 10000) {
      ofile << stvoriUlaznePodatke();
    }
    else {
      ofile << "\n" << stvoriUlaznePodatke();
    }
  }
  ofile.close();

/*
  //#|#|#|#|#|#|#|#|#|# S E G M E N T 2 #|#|#|#|#|#|#|#|#|#
  std::list<char>struktura1;

  std::list<int>::iterator it;

  //std::chrono::time_point<std::chrono::high_resolution_clock> t1 = std::chrono::high_resolution_clock::now();
  std::ifstream ifile("ulaz.csv");
  std::string line;
  if(ifile.is_open()) {
    while (getline(ifile, line)) {  
    char arr[line.length() + 1]; 
    for (int x = 0; x < sizeof(arr); x++) { 
        arr[x] = line[x]; 
    } 
    // insert pocetak
    //struktura1.push_front(arr[0]);
    // insert sredina
    //struktura1.insert(it, 2, arr[0]);
    // insert kraj
    struktura1.push_back(arr[0]);

  }
    ifile.close();
  }
  std::chrono::time_point<std::chrono::high_resolution_clock> t1 = std::chrono::high_resolution_clock::now();

  // access
  
  //auto prviElement = struktura1.begin();
  //std::advance(prviElement, 999);
  //std::cout << "1000. element " << *prviElement << std::endl;
  

  // brisanje elemenata
  auto it1 = struktura1.begin();
  std::advance(it1, 4500);
  auto it2 = struktura1.begin();
  std::advance(it2, 5500);
  
  struktura1.erase(it1,it2);
  
  //for(int i = 0; i < 1000; i++) {
    //struktura1.pop_front(); // prvi 1000 elemenata
    //struktura1.pop_back(); // zadnjih 1000 elemenata
  //}
  
  std::chrono::time_point<std::chrono::high_resolution_clock> t2 = std::chrono::high_resolution_clock::now();

  std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);

  long long trajanje = ms.count();
  std::cout << "Trajanje zadatka" << trajanje <<"ms"<<std::endl;

  // prikaz liste
  //for (auto s : struktura1) {
   // std::cout << s << std::endl;
  //}

*/
  //#|#|#|#|#|#|#|#|#|# S E G M E N T 3 #|#|#|#|#|#|#|#|#|#

  //std::array<char, 10000> struktura2;

  std::array<char, 15000> struktura2; // insert sredina

  std::chrono::time_point<std::chrono::high_resolution_clock> t3 = std::chrono::high_resolution_clock::now();
  std::ifstream ifile2("ulaz.csv");
  std::string line2;
  int z = 0; // insert pocetak
  //int z = 9999; // insert kraj
  if(ifile2.is_open()) {        
    while (getline(ifile2, line2)) {  
    char arr[line2.length() + 1]; 
      for (int x = 0; x < sizeof(arr); x++) { 
        arr[x] = line2[x]; 
      }  
      struktura2[z+500] = arr[0];
      z++; //insert pocetak
      //z--;
    }
    ifile2.close();
  }
  /*for(int i = 0; i < sizeof(struktura2); i++) {
    std::cout << struktura2[i];
  }*/
  
  //std::chrono::time_point<std::chrono::high_resolution_clock> t3 = std::chrono::high_resolution_clock::now();

  // access
  //std::cout << struktura2[1000];
  //std::array<char, 9000> struktura2obrisano;
  
  // brisanje zadnjih 1000 elemenata
  //for (int i = 0; i < 9000; i++) {
  //  struktura2obrisano[i] = struktura2[i]; 
  //}
  // brisanje prvih 1000 elemenata
  //int j = 0;
  //for (int i = 1000; i < 10000; i++) {
    //struktura2obrisano[j] = struktura2[i]; 
    //j++;
  //}
  // brisanje 1000 elemenata iz sredine
  /*for(int i = 0; i < 4500; i++) {
    struktura2obrisano[i] = struktura2[i];
  }
  int j = 4500;
  for(int i = 5500; i < 10000; i++) {
    struktura2obrisano[j] = struktura2[i];
    j++;
  }*/

  std::chrono::time_point<std::chrono::high_resolution_clock> t4 = std::chrono::high_resolution_clock::now();

  std::chrono::milliseconds ms2 = std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3);

  long long trajanje2 = ms2.count();
  std::cout << "Trajanje zadatka" << trajanje2 <<"ms"<<std::endl;

  /* prikaz polja
  for(int i = 0; i < sizeof(struktura2); i++) {
    std::cout << struktura2[i];
  }
  */

  /*for(int i = 0; i < sizeof(struktura2obrisano); i++) {
    std::cout << i << struktura2obrisano[i];
  }*/


  //#|#|#|#|#|#|#|#|#|# S E G M E N T 5 #|#|#|#|#|#|#|#|#|#

  // posebna datoteka radi preglednosti 
  
}

