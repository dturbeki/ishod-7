# Ishod 7 

**Segment 2**

| Zadatak           | Vrijeme       |
| ----------------- | -------------:|
| insert pocetak    | 4ms           |
| insert sredina    | ms            |
| insert kraj       | 3ms           |
| access 1000       | 0ms           |
| delete pocetak    | 0ms           |
| delete sredina    | 0ms           |
| delete kraj       | 0ms           |

**Segment 3**

| Zadatak           | Vrijeme       |
| ----------------- | -------------:|
| insert pocetak    | 1ms           |
| insert sredina    | 2ms           |
| insert kraj       | 1ms           |
| access 1000       | 0ms           |
| delete pocetak    | 0ms           |
| delete sredina    | 0ms           |
| delete kraj       | 0ms           |

**Segment 4**

● U kojim slučajevima je struktura 1 bolja ili lošija i zašto?
        Struktura 1 je bolja sa dinamičkim brojem elemenata, ako želimo više elemenata samo ćemo ih dodati, dok kod strukture 2 moramo napraviti dodatne korake. Za isti broj elemenata lista koristi više memorije, ali fleksibilnost s brojem podataka može doprinjeti tome da lista zapravo koristi manje memorije od polja. 

● U kojim slučajevima je struktura 2 bolja ili lošija i zašto?
        Struktura 2 je bolja sa upisom podataka sa fiksnim brojem elemenata jer su podaci bliže jedni drugima u cache-u. Pristup je veoma jednostavan i brz zbog korištenja indeksa. Ako želimo unijeti nove podatke ( povećati veličinu polja ) to može biti puno zahtjevnije od korištenja liste.

● U kojim slučajevima su vremena podjednaka i zašto?
        Vremena su podjednaka kod pristupa podataka i prilikom brisanja. Zbog malog broja podataka pristup i brisanje je podjednako.
        
● Odgovaraju li rezultati službenim mjerenjima a priori (cppreference)?
        U teoriji pristup podacima kod liste bi trebao biti sporiji jer ne možemo koristiti indeks kao kod bolja, tako da bismo došli do X elementa moramo proći ( traverse ) kroz sve prethodne. Brisanje bi trebalo biti bolje kod lista jer nisu potrebni dodatni međukoraci kao kod polja ( stvaranje novog polja sa manjim brojem elemenata, pomicanje elemenata u lijevo,.. )

**Segment 5**

Izradio sam algoritam za traženje proizvoljnog stringa u polju. Prolazi kroz polje i ako pronađe prvo slovo zadanog stringa povećava varijablu za 1. Kada dođe sljedeća vrijednost u polju s indeksom i, uspoređuje ju s drugim slovom u zadanom stringu jer je j sada 1. Ako je j == 3 znači da je pronađen niz slova koji odgovaraju stringu i program vraća true. Ako se niz ne pronađe vraća se false. Zbog problema sa pretraživanjem 10000 elemenata kod je izdvojen u posebnu datoteku segment5.cpp u kojem je broj ulaznih elemenata smanjen na 1000.

● Kada je ovaj algoritam idealan?
Kada se riječ nalazi na početku polja. Ako je veličina polja mala.

● Kada je ova algoritam loš izbor?
Kada se riječ nalazi na kraju polja. Ako je veličina polja velika morati. Meni je osobno nakon 9000 podatka izbacivao segmentation fault dok je na 1000 podataka radio bez problema.

● Kakav je u usporedbi sa sličnim algoritmima?
Postoje bolje opcije za traženje stringa kao što su Knuth-Morris-Pratt algoritam ( O(n) ), Bitap algoritam ( O(mn) ), BOM algoritam ( O(mn) ) i mnogi drugi. 

