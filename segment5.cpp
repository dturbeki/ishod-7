#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <cstring>
#include <vector>
#include <bits/stdc++.h>
#include <algorithm>
#include <array>

const int MAX = 26;


std::string stvoriUlaznePodatke() { 
  char abc[MAX] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                    'h', 'i', 'j', 'k', 'l', 'm', 'n',  
                    'o', 'p', 'q', 'r', 's', 't', 'u', 
                    'v', 'w', 'x', 'y', 'z' }; 
  
  std::string znak; 
  znak += abc[rand() % MAX];   
  return znak; 
} 
bool pronađiNiz(std::array<char,1000> struktura2, std::string str, int i, int j) {
  int n = str.length();

  char char_array[n+1];
  strcpy(char_array, str.c_str());
  for (int x = 0; x < sizeof(char_array); x++) { 
    char_array[x] = str[x]; 
  } 
  if(i < 1000) {
    if(struktura2[i] == char_array[j]) {
      j++;
      if(j == 3) {
        return true;
      } 
      else {
        return pronađiNiz(struktura2, str, i+1, j);
      }
    }
    else {
      return pronađiNiz(struktura2, str, i+1, 0);
    }
  }
  return false;
  
}
int main() {
  //#|#|#|#|#|#|#|#|#|# S E G M E N T 1 #|#|#|#|#|#|#|#|#|#
  srand(time(NULL)); 

  std::ofstream ofile("ulaz.csv");
  for (int counter = 0; counter < 1000; counter++) {
    if(counter == 0 || counter == 1000) {
      ofile << stvoriUlaznePodatke();
    }
    else {
      ofile << "\n" << stvoriUlaznePodatke();
    }
  }
  ofile.close();

  std::array<char, 1000> struktura2;

  std::ifstream ifile2("ulaz.csv");
  std::string line2;
  int z = 0;
  if(ifile2.is_open()) {
    while (getline(ifile2, line2)) {  
    char arr[line2.length() + 1]; 
      for (int x = 0; x < sizeof(arr); x++) { 
        arr[x] = line2[x]; 
      }  
      struktura2[z] = arr[0];
      z++;
    }
    ifile2.close();
  }

  //#|#|#|#|#|#|#|#|#|# S E G M E N T 5 #|#|#|#|#|#|#|#|#|#

  for(int i = 0; i < sizeof(struktura2); i++) {
    std::cout << struktura2[i];
  }

  std::string str = "abc";
  if(pronađiNiz(struktura2, str, 0, 0)) {
    std::cout << "\nNiz " << str << " pronađen" << std::endl;
  }
  else {
    std::cout << "\nNiz " << str << " nije pronađen" << std::endl;
  }
  
}
